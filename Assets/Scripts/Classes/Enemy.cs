﻿using CoAHomework;
using UnityEngine;

namespace Classes
{
    public class Enemy : MonoBehaviour, IDamagable, IHealable, ILog
    {
        [SerializeField]
        private int currentHealth;

        private void Start()
        {
            Damage(currentHealth * 2);
            Heal(currentHealth / 2);
        }
        
        public void Log(object message)
        {
            Debug.Log("[Enemy] " + message);
        }
        
        public void Damage(int amount)
        {
            currentHealth -= amount;
            Log("Damage dealt: " + amount);
        }

        public void Heal(int amount)
        {
            currentHealth += amount;
            Log("Healing by: " + amount);
        }
    }
}