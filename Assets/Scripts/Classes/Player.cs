﻿using System;
using CoAHomework;
using UnityEngine;

namespace Classes
{
    public class Player : MonoBehaviour, IDamagable, IHealable, ILog
    {
        [SerializeField]
        private int currentHealth;
        [SerializeField]
        private int damage;
        [SerializeField]
        private int heal;
        
        private void Start()
        {
            Damage(damage);
            Heal(heal);
        }
        
        public void Log(object message)
        {
            Debug.Log("[Player] " + message);
        }
        
        public void Damage(int amount)
        {
            currentHealth -= amount;
            Log("Damage dealt: " + amount);
        }

        public void Heal(int amount)
        {
            currentHealth += amount;
            Log("Healing by: " + amount);
        }
    }
}