﻿namespace CoAHomework
{
    public interface IDamagable
    {
        void Damage(int amount);
    }
}