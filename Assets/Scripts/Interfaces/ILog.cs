﻿namespace CoAHomework
{
    public interface ILog
    {
        void Log(object message);
    }
}