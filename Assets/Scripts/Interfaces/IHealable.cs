﻿namespace CoAHomework
{
    public interface IHealable
    {
        void Heal(int amount);
    }
}